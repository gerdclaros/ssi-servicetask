/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Item;
import com.dh.ssiservice.model.Training;
import org.apache.tomcat.util.codec.binary.Base64;

public class TrainingCommand {

    private Long id;
    private String skill;
    private String date;
    private String area;
    private String position;
    private String trainingReg;

    public TrainingCommand(Training training) {
        this.setArea(training.getArea().getName());
        this.setSkill(training.getSkill());
        this.setId(training.getId());


    }

    public TrainingCommand() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTrainingReg() {
        return trainingReg;
    }

    public void setTrainingReg(String trainingReg) {
        this.trainingReg = trainingReg;
    }

    public Training toDomain() {
        Training training = new Training();
        training.setId(getId());
        training.setSkill(getSkill());

        return training;
    }
}
