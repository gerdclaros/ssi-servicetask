/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.RegIncidente;

public class RegIncidenteCommand {


    private Long id;
    private String incident;
    private String employee;
    private String area;
    private String causa;
    private Long cuantificador;

    public RegIncidenteCommand(RegIncidente regIncidente) {
        this.setArea(regIncidente.getArea().getName());
        this.setCausa(regIncidente.getCausa());
        this.setId(regIncidente.getId());
        this.setEmployee(regIncidente.getEmployee().getFirstName());
    }

    public RegIncidenteCommand() {

    }

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCausa() {
        return causa;
    }

    public void setCausa(String causa) {
        this.causa = causa;
    }

    public Long getCuantificador() {
        return cuantificador;
    }

    public void setCuantificador(Long cuantificador) {
        this.cuantificador = cuantificador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RegIncidente toDomain() {
        RegIncidente regIncidente = new RegIncidente();
        regIncidente.setId(getId());
        regIncidente.setCausa(getCausa());
        regIncidente.setCuantificador(getCuantificador());

        return regIncidente;
    }
}
