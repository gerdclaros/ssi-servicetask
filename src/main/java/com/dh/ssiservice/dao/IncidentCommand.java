/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.Incident;

public class IncidentCommand {

    private String nombre;
    private String codigo;
    private Long id;
    private String subCategoryIncidente;

    public IncidentCommand(Incident incident) {
        this.setNombre(incident.getNombre());
        this.setCodigo(incident.getCodigo());
        this.setSubCategoryIncidente(incident.getSubCategoryIncidente().getCode());

    }

    public IncidentCommand() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSubCategoryIncidente() {
        return subCategoryIncidente;
    }

    public void setSubCategoryIncidente(String subCategoryIncidente) {
        this.subCategoryIncidente = subCategoryIncidente;
    }

    public Incident toDomain() {
        Incident incident = new Incident();
        incident.setId(getId());
        incident.setCodigo(getCodigo());
        incident.setNombre(getNombre());
        return incident;
    }
}
