/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.Item;
import com.dh.ssiservice.model.Training;

import javax.persistence.OneToOne;

public class InventoryCommand {

    private String almacen;
    private String item;
    private Long cantidad;
    private String estado;
    private Long id;

    public InventoryCommand(Inventory inventory) {

        this.setAlmacen(inventory.getAlmacen());
        this.setItem(inventory.getItem().getName());
        this.setEstado(inventory.getEstado());
        this.setCantidad(inventory.getCantidad());
        this.setId(inventory.getId());

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InventoryCommand() {

    }

    public String getAlmacen() {
        return almacen;
    }

    public void setAlmacen(String almacen) {
        this.almacen = almacen;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Inventory toDomain() {
        Inventory inventory = new Inventory();
        inventory.setId(getId());
        inventory.setAlmacen(getAlmacen());

        inventory.setCantidad(getCantidad());
        inventory.setEstado(getEstado());
        return inventory;
    }
}
