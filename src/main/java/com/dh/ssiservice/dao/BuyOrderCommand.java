/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.Training;

public class BuyOrderCommand {

    private Long id;
    private String unidad;
    private String item;
    private String provider;
    private String estado;

    public BuyOrderCommand(BuyOrder buyOrder) {
        this.setItem(buyOrder.getItem().getName());
        this.setEstado(buyOrder.getEstado());
        this.setProvider(buyOrder.getProvider().getFirstName());
        this.setUnidad(buyOrder.getUnidad());


    }

    public BuyOrderCommand() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public BuyOrder toDomain() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setId(getId());
        buyOrder.setEstado(getEstado());
        buyOrder.setUnidad(getUnidad());

        return buyOrder;
    }
}
