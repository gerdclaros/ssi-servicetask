package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.RegIncidente;

public interface RegIncidenteRepository extends CrudRepository<RegIncidente, Long> {
}

    