package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Provider;

public interface ProviderRepository extends CrudRepository<Provider, Long> {
}

    