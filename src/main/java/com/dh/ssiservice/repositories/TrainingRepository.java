package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Category;
import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Training;

import java.util.List;
import java.util.Optional;

public interface TrainingRepository extends CrudRepository<Training, Long> {

}

    