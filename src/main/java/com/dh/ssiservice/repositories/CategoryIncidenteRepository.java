package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.CategoryIncidente;

public interface CategoryIncidenteRepository extends CrudRepository<CategoryIncidente, Long> {
}

    