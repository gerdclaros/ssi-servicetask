package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.TrainingReg;

public interface TrainingRegRepository extends CrudRepository<TrainingReg, Long> {
}

    