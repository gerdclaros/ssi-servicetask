package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.BuyOrder;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
}

    