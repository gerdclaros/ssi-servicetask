package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Incident;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}

    