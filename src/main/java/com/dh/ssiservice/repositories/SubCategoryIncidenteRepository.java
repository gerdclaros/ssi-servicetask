package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.SubCategoryIncidente;

public interface SubCategoryIncidenteRepository extends CrudRepository<SubCategoryIncidente, Long> {
}

    