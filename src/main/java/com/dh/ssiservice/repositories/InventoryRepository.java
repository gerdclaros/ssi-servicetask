package com.dh.ssiservice.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservice.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
}

    