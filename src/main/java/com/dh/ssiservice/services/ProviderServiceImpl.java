package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Provider;
import com.dh.ssiservice.repositories.ProviderRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class ProviderServiceImpl extends GenericServiceImpl<Provider> implements ProviderService {
    private ProviderRepository repository;

    public ProviderServiceImpl(ProviderRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Provider, Long> getRepository() {
        return repository;
    }
}