package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.repositories.InventoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class InventoryServiceImpl extends GenericServiceImpl<Inventory> implements InventoryService {
    private InventoryRepository repository;

    public InventoryServiceImpl(InventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Inventory, Long> getRepository() {
        return repository;
    }
}