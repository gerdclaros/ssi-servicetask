package com.dh.ssiservice.services;

import com.dh.ssiservice.model.TrainingReg;
import com.dh.ssiservice.repositories.TrainingRegRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TrainingRegServiceImpl extends GenericServiceImpl<TrainingReg> implements TrainingRegService {
    private TrainingRegRepository repository;

    public TrainingRegServiceImpl(TrainingRegRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<TrainingReg, Long> getRepository() {
        return repository;
    }
}