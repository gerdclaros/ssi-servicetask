package com.dh.ssiservice.services;

import com.dh.ssiservice.model.RegIncidente;

public interface RegIncidenteService extends GenericService<RegIncidente> {
}
