package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.repositories.BuyOrderRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class BuyOrderServiceImpl extends GenericServiceImpl<BuyOrder> implements BuyOrderService {
    private BuyOrderRepository repository;

    public BuyOrderServiceImpl(BuyOrderRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<BuyOrder, Long> getRepository() {
        return repository;
    }
}