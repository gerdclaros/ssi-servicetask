package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Provider;

public interface ProviderService extends GenericService<Provider> {
}
