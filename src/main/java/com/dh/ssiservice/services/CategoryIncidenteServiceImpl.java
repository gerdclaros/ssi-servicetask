package com.dh.ssiservice.services;

import com.dh.ssiservice.model.CategoryIncidente;
import com.dh.ssiservice.repositories.CategoryIncidenteRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryIncidenteServiceImpl extends GenericServiceImpl<CategoryIncidente> implements CategoryIncidenteService {
    private CategoryIncidenteRepository repository;

    public CategoryIncidenteServiceImpl(CategoryIncidenteRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<CategoryIncidente, Long> getRepository() {
        return repository;
    }
}