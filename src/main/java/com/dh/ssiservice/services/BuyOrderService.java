package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;
import org.springframework.stereotype.Service;

@Service
public interface BuyOrderService extends GenericService<BuyOrder> {
}
