package com.dh.ssiservice.services;

import com.dh.ssiservice.model.CategoryIncidente;

public interface CategoryIncidenteService extends GenericService<CategoryIncidente> {
}
