package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.repositories.TrainingRepository;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.repositories.TrainingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TrainingServiceImpl extends GenericServiceImpl<Training> implements TrainingService {
    private TrainingRepository repository;

    public TrainingServiceImpl(TrainingRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Training, Long> getRepository() {
        return repository;
    }
}