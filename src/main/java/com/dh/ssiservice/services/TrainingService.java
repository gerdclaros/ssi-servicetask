package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Training;
import org.springframework.stereotype.Service;

@Service
public interface TrainingService extends GenericService<Training> {
}
