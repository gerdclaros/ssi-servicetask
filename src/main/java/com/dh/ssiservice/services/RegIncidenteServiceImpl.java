package com.dh.ssiservice.services;

import com.dh.ssiservice.model.RegIncidente;
import com.dh.ssiservice.repositories.RegIncidenteRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class RegIncidenteServiceImpl extends GenericServiceImpl<RegIncidente> implements RegIncidenteService {
    private RegIncidenteRepository repository;

    public RegIncidenteServiceImpl(RegIncidenteRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<RegIncidente, Long> getRepository() {
        return repository;
    }
}