package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.repositories.IncidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {
    private IncidentRepository repository;

    public IncidentServiceImpl(IncidentRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return repository;
    }
}