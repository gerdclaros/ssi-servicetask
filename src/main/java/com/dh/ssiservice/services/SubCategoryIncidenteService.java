package com.dh.ssiservice.services;

import com.dh.ssiservice.model.SubCategoryIncidente;

public interface SubCategoryIncidenteService extends GenericService<SubCategoryIncidente> {
}
