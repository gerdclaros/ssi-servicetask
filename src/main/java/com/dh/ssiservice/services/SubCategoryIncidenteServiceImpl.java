package com.dh.ssiservice.services;

import com.dh.ssiservice.model.SubCategoryIncidente;
import com.dh.ssiservice.repositories.SubCategoryIncidenteRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class SubCategoryIncidenteServiceImpl extends GenericServiceImpl<SubCategoryIncidente> implements SubCategoryIncidenteService {
    private SubCategoryIncidenteRepository repository;

    public SubCategoryIncidenteServiceImpl(SubCategoryIncidenteRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<SubCategoryIncidente, Long> getRepository() {
        return repository;
    }
}