package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;
import org.springframework.stereotype.Service;

@Service
public interface IncidentService extends GenericService<Incident> {
}
