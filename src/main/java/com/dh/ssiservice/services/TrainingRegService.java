package com.dh.ssiservice.services;

import com.dh.ssiservice.model.TrainingReg;
import org.springframework.stereotype.Service;

@Service
public interface TrainingRegService extends GenericService<TrainingReg> {
}
