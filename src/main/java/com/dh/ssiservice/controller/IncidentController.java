package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentCommand;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/incidents")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class IncidentController {
    private IncidentService service;

    public IncidentController(IncidentService service) {
        this.service = service;
    }

    @GET
    public Response getIncidents() {

        List<IncidentCommand> incidentCommandList = new ArrayList<>();
        service.findAll().forEach(buyOrders -> {
            IncidentCommand incidentCommand = new IncidentCommand(buyOrders);
            incidentCommandList.add(incidentCommand);
        });
        return Response.ok(incidentCommandList).build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentsById(@PathVariable("id") @NotNull Long id) {
        Incident incident = service.findById(id);
        return Response.ok(new IncidentCommand(incident)).build();

    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
