package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.TrainingRegService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Controller
@Path("/trainingRegs")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class TrainingRegController {
    private TrainingRegService service;

    public TrainingRegController(TrainingRegService service) {
        this.service = service;
    }

    @GET
    public String getTrainingRegs(Model model) {
        model.addAttribute("trainingRegs", service.findAll());
        return "trainingRegs";
    }

    @GET
    @Path("/{id}")
    public String getTrainingRegsById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("trainingReg", service.findById(id));
        return "trainingReg";
    }
}
