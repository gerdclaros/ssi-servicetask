package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.SubCategoryIncidenteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/subCategoryIncidentes")
public class SubCategoryIncidenteController {
    private SubCategoryIncidenteService service;

    public SubCategoryIncidenteController(SubCategoryIncidenteService service) {
        this.service = service;
    }

    @RequestMapping
    public String getSubCategoryIncidentes(Model model) {
        model.addAttribute("subCategoryIncidentes", service.findAll());
        return "subCategoryIncidentes";
    }

    @RequestMapping("/{id}")
    public String getSubCategoryIncidentesById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("subCategoryIncidente", service.findById(id));
        return "subCategoryIncidente";
    }
}
