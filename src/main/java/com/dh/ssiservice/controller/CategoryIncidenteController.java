package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.CategoryIncidenteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/categoryIncidentes")
public class CategoryIncidenteController {
    private CategoryIncidenteService service;

    public CategoryIncidenteController(CategoryIncidenteService service) {
        this.service = service;
    }

    @RequestMapping
    public String getCategoryIncidentes(Model model) {
        model.addAttribute("categoryIncidentes", service.findAll());
        return "categoryIncidentes";
    }

    @RequestMapping("/{id}")
    public String getCategoryIncidentesById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("categoryIncidente", service.findById(id));
        return "categoryIncidente";
    }
}
