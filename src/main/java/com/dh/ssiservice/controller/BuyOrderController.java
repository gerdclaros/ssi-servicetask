package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.BuyOrderCommand;
import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/buyOrders")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class BuyOrderController {
    private BuyOrderService service;

    public BuyOrderController(BuyOrderService service) {
        this.service = service;
    }

    @GET
    public Response getBuyOrders() {
        List<BuyOrderCommand> buyOrderCommandList = new ArrayList<>();
        service.findAll().forEach(buyOrders -> {
            BuyOrderCommand buyOrderCommand = new BuyOrderCommand(buyOrders);
            buyOrderCommandList.add(buyOrderCommand);
        });
        return Response.ok(buyOrderCommandList).build();
    }


    @GET
    @Path("/{id}")
    public Response getBuyOrdersById(@PathVariable("id") @NotNull Long id) {

        BuyOrder buyOrder = service.findById(id);
        return Response.ok(new BuyOrderCommand(buyOrder)).build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
