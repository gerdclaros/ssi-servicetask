package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.ItemCommand;
import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Item;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.TrainingRegService;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/trainings")
@Produces(MediaType.APPLICATION_JSON)
@CrossOrigin
public class TrainingController {
    private TrainingService trainingService;
    private TrainingRegService trainingRegService;

    public TrainingController(TrainingService trainingService, TrainingRegService trainingRegService) {
        this.trainingService = trainingService;
        this.trainingRegService = trainingRegService;
    }

    @GET
    public Response getTrainings() {

        List<TrainingCommand> trainings = new ArrayList<>();
        trainingService.findAll().forEach(training -> {
            TrainingCommand trainingCommand = new TrainingCommand(training);
            trainings.add(trainingCommand);
        });
        return Response.ok(trainings).build();


    }

    @GET
    @Path("/{id}")
    public Response getTrainingsById(@PathVariable("id") @NotNull Long id) {
        Training training = trainingService.findById(id);
        return Response.ok(new TrainingCommand(training)).build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
