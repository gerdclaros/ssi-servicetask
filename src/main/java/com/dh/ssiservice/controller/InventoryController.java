package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.services.InventoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/inventorys")
@Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
@CrossOrigin
public class InventoryController {

    private InventoryService service;

    public InventoryController(InventoryService service) {
        this.service = service;
    }

    @GET
    public Response getInventorys() {

        List<InventoryCommand> inventoryCommands = new ArrayList<>();
        service.findAll().forEach(inventory -> {

            InventoryCommand inventoryC = new InventoryCommand(inventory);
            inventoryCommands.add(inventoryC);
        });
        return Response.ok(inventoryCommands).build();
    }


    @GET
    @Path("/{id}")
    public Response getInventorysById(@PathVariable("id") @NotNull Long id) {
        Inventory inventory = service.findById(id);
        return Response.ok(new InventoryCommand(inventory)).build();
    }
}
