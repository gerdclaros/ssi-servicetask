package com.dh.ssiservice.controller;

import com.dh.ssiservice.services.RegIncidenteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/regIncidentes")
public class RegIncidenteController {
    private RegIncidenteService service;

    public RegIncidenteController(RegIncidenteService service) {
        this.service = service;
    }

    @RequestMapping
    public String getRegIncidentes(Model model) {
        model.addAttribute("regIncidentes", service.findAll());
        return "regIncidentes";
    }

    @RequestMapping("/{id}")
    public String getRegIncidentesById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("regIncidente", service.findById(id));
        return "regIncidente";
    }
}
