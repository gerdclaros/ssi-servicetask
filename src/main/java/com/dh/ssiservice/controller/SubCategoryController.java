package com.dh.ssiservice.controller;

import com.dh.ssiservice.repositories.SubCategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SubCategoryController {


    private SubCategoryRepository subcategoryRepository;

    public SubCategoryController(SubCategoryRepository subcategoryRepository) {
        this.subcategoryRepository = subcategoryRepository;
    }

    @RequestMapping("/subcategories")
    public String getSubCategories(Model model) {
        model.addAttribute("subcategories", subcategoryRepository.findAll());
        return "subcategories";
    }


}
