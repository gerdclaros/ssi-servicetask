package com.dh.ssiservice.controller;

import com.dh.ssiservice.repositories.ContractRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Controller
@RequestMapping("/contracts")
public class ContractController {
    private ContractRepository contractRepository;


    public ContractController(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }


    @GET
    public String getContract(@RequestParam(value = "id", required = false) Long id, Model model) {
        model.addAttribute("contracts", StringUtils.isEmpty(id) ?
                contractRepository.findAll() :
                contractRepository.findById(id));
        //contractRepository.findById(employeeRepository.findByFirstName(firstName)).get());
        return "contracts";
    }

    @GET
    @Path("/{id}")
    public String getContractById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("contract", contractRepository.findById(id).get());
        return "contract";
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
