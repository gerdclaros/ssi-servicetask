/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class RegIncidente extends ModelBase {

    @OneToOne(optional = false)
    private Incident incident;

    @OneToOne(optional = false)
    private Employee employee;
    @OneToOne(optional = false)
    private Area area;
    private String causa;
    private Long cuantificador;

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getCausa() {
        return causa;
    }

    public void setCausa(String causa) {
        this.causa = causa;
    }

    public Long getCuantificador() {
        return cuantificador;
    }

    public void setCuantificador(Long cuantificador) {
        this.cuantificador = cuantificador;
    }


}
