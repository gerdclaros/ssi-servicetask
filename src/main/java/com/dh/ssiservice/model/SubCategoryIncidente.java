/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class SubCategoryIncidente extends ModelBase {
    private String name;
    private String code;
    @OneToOne(optional = false)
    private CategoryIncidente categoryIncidente;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategoryIncidente getCategoryIncidente() {
        return categoryIncidente;
    }

    public void setCategoryIncidente(CategoryIncidente categoryIncidente) {
        this.categoryIncidente = categoryIncidente;
    }
}
