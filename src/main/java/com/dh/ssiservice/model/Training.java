/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import javax.persistence.*;

@Entity
public class Training extends ModelBase {



    private String skill;
    private String date;
    @OneToOne(optional = false)
    private Area area;
    @OneToOne(optional = false)
    private Position position;

    @OneToOne(optional = false)
    private TrainingReg trainingReg;

    public TrainingReg getTrainingReg() {
        return trainingReg;
    }

    public void setTrainingReg(TrainingReg trainingReg) {
        this.trainingReg = trainingReg;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }



}
