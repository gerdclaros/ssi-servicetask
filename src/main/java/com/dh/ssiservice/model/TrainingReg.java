package com.dh.ssiservice.model;


import javax.persistence.*;

@Entity
public class TrainingReg extends ModelBase {

    @OneToOne(optional = false)
    private Employee employee;
    @OneToOne(optional = false)
    private Training training;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }


}
