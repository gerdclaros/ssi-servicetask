package com.dh.ssiservice.model;

import javax.persistence.*;

@Entity
public class BuyOrder extends ModelBase {

    private String unidad;

    @OneToOne(optional = false)
    private Item item;
    @OneToOne(optional = false)
    private Provider provider;
    private String estado;

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


}
