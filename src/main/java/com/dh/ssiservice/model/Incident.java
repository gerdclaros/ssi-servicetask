/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Incident extends ModelBase {


    private String nombre;
    private String codigo;
    @OneToOne(optional = false)
    private SubCategoryIncidente subCategoryIncidente;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public SubCategoryIncidente getSubCategoryIncidente() {
        return subCategoryIncidente;
    }

    public void setSubCategoryIncidente(SubCategoryIncidente subCategoryIncidente) {
        this.subCategoryIncidente = subCategoryIncidente;
    }


}
